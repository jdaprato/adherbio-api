<?php

namespace App\Tests;

use Ramsey\Uuid\UuidFactory;
use Ramsey\Uuid\UuidInterface;

class MyTestUuidFactory extends UuidFactory
{
    public $uuid1;

    public function uuid1($node = null, ?int $clockSeq = null): UuidInterface
    {
        return $this->uuid1;
    }
}