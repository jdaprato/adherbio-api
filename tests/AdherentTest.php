<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Adherent;
use Faker\Provider\Uuid;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class AdherentTest extends ApiTestCase
{

    use RefreshDatabaseTrait;

    public function testGetCollection(): void
    {
        $response = static::createClient()->request('GET', '/api/adherents');
        // Check the response type : here 200 wanted
        $this->assertResponseStatusCodeSame(200);
        // Check the header

        $this->assertJsonContains([
            '@context' => '/contexts/Adherent',
            '@id' => '/adherents',
            '@type' => 'hydra:Collection',
            'hydra:totalItems' => 20,
            'hydra:view' => [
                '@id' => '/adherents?page=1',
                '@type' => 'hydra:PartialCollectionView',
                'hydra:first' => '/adherents?page=1',
                'hydra:last' => '/adherents?page=4',
                'hydra:next' => '/adherents?page=2',
            ],
        ]);

        $this->assertCount(30, $response->toArray()['hydra:member']);
        $this->assertMatchesResourceCollectionJsonSchema(Adherent::class);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testTellTime(): void
    {
        $factory = new MyTestUuidFactory();
        Uuid::setFactory($factory);

        $myObj = new Adherent();

        $factory->uuid1 = Uuid::fromString('177ef0d8-6630-11ea-b69a-0242ac130003');
        $this->assertSame('2020-03-14 20:12:12', $myObj->tellTime());

        $factory->uuid1 = Uuid::fromString('13814000-1dd2-11b2-9669-00007ffffffe');
        $this->assertSame('1970-01-01 00:00:00', $myObj->tellTime());
    }
}
