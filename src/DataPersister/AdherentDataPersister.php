<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Adherent;

final class AdherentDataPersister implements DataPersisterInterface
{

  private $em;

  public function __construct(EntityManagerInterface $em)
  {
    $this->em = $em;
  }

  public function supports($data, array $context = []): bool
  {
    return $data instanceof Adherent;
  }

  public function persist($data, array $context = [])
  {
    $this->em->persist($data);
    $this->em->flush();
  }

  public function remove($data, array $context = [])
  {
    $this->em->remove($data);
    $this->em->flush();
  }
}