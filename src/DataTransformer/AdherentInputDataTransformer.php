<?php

namespace App\DataTransformer;

use Ramsey\Uuid\Uuid;
use App\Entity\Adherent;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use DateTime;
use ApiPlatform\Core\Validator\ValidatorInterface;

final class AdherentInputDataTransformer implements DataTransformerInterface
{

    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
        
    }
    /**
     * {@inheritdoc}
     */
    public function transform($data, string $to, array $context = [])
    {
        $this->validator->validate($data);
        $adherent = new Adherent();
        $adherent->setId(Uuid::uuid4());
        $adherent->setName($data->name);
        $adherent->setDescription($data->description);
        $adherent->setPhone($data->phone);
        $adherent->setEmail($data->email);
        $adherent->setSiren($data->siren);
        $adherent->setActive(false);
        $adherent->setCreatedAt(new DateTime());
        $adherent->setUpdatedAt(new DateTime());
        
        return $adherent;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        // in the case of an input, the value given here is an array (the JSON decoded).
        // if it's a adherent we transformed the data already
        if ($data instanceof Adherent) {
            return false;
        }

        return Adherent::class === $to && null !== ($context['input']['class'] ?? null);
    }
}
