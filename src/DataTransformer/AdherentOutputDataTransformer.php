<?php

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\AdherentOutput;
use App\Entity\Adherent;

final class AdherentOutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($data, string $to, array $context = [])
    {
        $output = new AdherentOutput();
        $output->id = $data->getId();
        $output->name = $data->getName();
        $output->description = $data->getDescription();
        $output->phone= $data->getPhone();
        $output->email = $data->getEmail();
        $output->active = $data->getActive();
        $output->contacts = $data->getContacts();
        $output->catalogs = $data->getCatalogs();

        return $output;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return AdherentOutput::class === $to && $data instanceof Adherent;
    }
}
