<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * From the API
 */
final class AdherentInput
{
    /**
     * @var string The name of this adherent
     * @Assert\Type("string")
     * @Assert\NotBlank(allowNull=false)
     */
    public $name;

    /**
     * @var string|null The description of this adherent or null if not given
     * @Assert\Type("string")
     */
    public $description;

    /**
     * @var string|null The phone of this adherent or null if not given
     * @Assert\Regex(pattern="/^((\+)33|0|0033)[1-9](\d{2}){4}$/")
     * @Assert\Type("string")
     */
    public $phone;

    /**
     * @var string|null The email of this adherent or null if not given
     * @Assert\Email()
     */
    public $email;

    /**
     * @var int|null The siren of this adherent or null if not given
     * @Assert\Length(
     *          min=9,
     *          max=9
     * )
     */
    public $siren;

    /**
     * @var bool If the adherent is active or not
     * @Assert\Type("bool")
     */
    public $active;

    /**
     * @var array The contacts the adherent has
     * @Assert\Type("array")
     */
    public $contacts;

    /**
     * @var array The catalogs linked to this adherent
     * @Assert\Type("array")
     */
    public $catalogs;

    /**
     * @var \DateTimeInterface Creation date of this adherent
     */
    public $createdAt;

    /**
     * @var \DateTimeInterface Last time this adherent's infos have been updated
     */
    public $updatedAt;
}
