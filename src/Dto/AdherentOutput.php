<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * From the ORM
 */
final class AdherentOutput
{
     /**
     * @var string The name of this adherent
     * @Assert\NotBlank(allowNull=false)
     * @Assert\Type("string")
     */
    public $name;

    /**
     * @var string|null The description of this adherent or null if not given
     */
    public $description;

    /**
     * @var string|null The phone of this adherent or null if not given
     */
    public $phone;

    /**
     * @var string|null The email of this adherent or null if not given
     */
    public $email;

    /**
     * @var int|null The siren of this adherent or null if not given
     */
    public $siren;

    /**
     * @var bool If the adherent is active or not
     */
    public $active;

    /**
     * @var array The contacts the adherent has
     */
    public $contacts;

    /**
     * @var array The catalogs linked to this adherent
     */
    public $catalogs;

    /**
     * @var \DateTimeInterface Creation date of this adherent
     */
    public $createdAt;

    /**
     * @var \DateTimeInterface Last time this adherent's infos have been updated
     */
    public $updatedAt;


}