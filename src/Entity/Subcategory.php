<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Mtarld\SymbokBundle\Annotation\Getter;
/**
 * @ORM\Entity
 * @ApiResource
 */
class SubCategory
{

    /**
     * @var string The id (uuid) of this subcategory
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @var string The name of this subcategory
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string The technicalName of this subcategory
     * @ORM\Column(type="string", length=255)
     */
    private $technicalName;

    /**
     * @var string|null The description of this subcategory or null if not given
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var string The category this subcategory belongs to
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="subcategories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @var \DateTimeInterface Creation date of this subcategory
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface Last time this subcategory's infos have been updated
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getTechnicalName(): ?string
    {
        return $this->technicalName;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }
}
