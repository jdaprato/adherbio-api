<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use DateTime;
use Mtarld\SymbokBundle\Annotation\Getter;
use PhpParser\Node\Expr\Cast\Array_;

/**
 * @ORM\Entity
 * @ApiResource
 */
class Catalog
{
    /**
     * @var string The id (uuid) of this catalog
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @var string The adherent this catalog belongs to
     * @ORM\ManyToOne(targetEntity=Adherent::class, inversedBy="catalogs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $adherent;

    /**
     * @var string The name of this catalog
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string The technicalName of this catalog
     * @ORM\Column(type="string", length=255)
     */
    private $technicalName;

    /**
     * @var string|null The description of this catalog or null if not given
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;


    /**
     * @var bool If the adherent is active or not
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @var array The categories linked to this catalog
     * @ORM\ManyToMany(targetEntity=Category::class, mappedBy="catalog")
     */
    private $categories;

    /**
     * @var \DateTimeInterface Creation date of this catalog
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface Last time this catalog's infos have been updated
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAdherent(): ?string
    {
        return $this->adherent;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getTechnicalName(): ?string
    {
        return $this->technicalName;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function getCategories(): ?Array
    {
        return $this->categories;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

}
