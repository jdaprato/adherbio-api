<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Mtarld\SymbokBundle\Annotation\Getter;

/**
 * @ORM\Entity
 * @ApiResource
 */
class Category
{

    /**
     * @var string The id (uuid) of this category
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @var string The name of this category
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string The technicalName of this category
     * @ORM\Column(type="string", length=255)
     */
    private $technicalName;

    /**
     * @var string|null The description of this category or null if not given
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var array The subcategories linked to this category
     * @ORM\OneToMany(targetEntity=Subcategory::class, mappedBy="category", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $subcategories;

    /**
     * @var \DateTimeInterface Creation date of this category
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface Last time this category's infos have been updated
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getTechnicalName(): ?string
    {
        return $this->technicalName;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function getSubcategories(): ?Array
    {
        return $this->subcategories;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }
}
