<?php

namespace App\Entity;

use App\Dto\AdherentInput;
use App\Dto\AdherentOutput;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Faker\Provider\Uuid;

/**
 * @ORM\Entity
 * @ApiResource(
 *   input=AdherentInput::class,
 *   output=AdherentOutput::class,
 *   denormalizationContext={"disable_type_enforcement"=true},
 * )
 */
class Adherent
{

    /**
     * @var \Ramsey\Uuid\UuidInterface The id (uuid) of this adherent
     * @ApiProperty(identifier=true)
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id;

    /**
     * @var string The name of this adherent
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string|null The description of this adherent or null if not given
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var string|null The phone of this adherent or null if not given
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string|null The email of this adherent or null if not given
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var int|null The siren of this adherent or null if not given
     * @ORM\Column(type="integer", nullable=true)
     */
    private $siren;

    /**
     * @var bool If the adherent is active or not
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @var array The contacts the adherent has
     * @ORM\OneToMany(targetEntity=Contact::class, mappedBy="adherent", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $contacts;

    /**
     * @var array The catalogs linked to this adherent
     * @ORM\OneToMany(targetEntity=Catalog::class, mappedBy="adherent", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $catalogs;

    /**
     * @var \DateTimeInterface Creation date of this adherent
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface Last time this adherent's infos have been updated
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    public function setId(\Ramsey\Uuid\UuidInterface $id)
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function getSiren(): ?int
    {
        return $this->siren;
    }

    public function setSiren(int $siren)
    {
        $this->siren = $siren;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active)
    {
        $this->active = $active;
    }


    public function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->catalogs = new ArrayCollection();
    }
    
    /**
     * @return Collection|Contact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }


    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setAdherent($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
            // set the owning side to null (unless already changed)
            if ($contact->getAdherent() === $this) {
                $contact->setAdherent(null);
            }
        }

        return $this;
    }
/**
     * @return Collection|Catalog[]
     */
    public function getCatalogs(): Collection
    {
        return $this->catalogs;
    }

    public function addCatalog(Catalog $catalog): self
    {
        if (!$this->catalogs->contains($catalog)) {
            $this->catalogs[] = $catalog;
            $catalog->setAdherent($this);
        }

        return $this;
    }

    public function removeCatalog(Catalog $catalog): self
    {
        if ($this->catalogs->contains($catalog)) {
            $this->catalogs->removeElement($catalog);
            // set the owning side to null (unless already changed)
            if ($catalog->getAdherent() === $this) {
                $catalog->setAdherent(null);
            }
        }

        return $this;
    }
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }


    public function setUpdatedAt(\DateTimeInterface $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}
