<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Mtarld\SymbokBundle\Annotation\Getter;

/**
 * @ORM\Entity
 * @ApiResource
 */
class Contact
{
    /**
     * @var string The id (uuid) of this contact
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @var string The adherent this contact belongs to
     * @ORM\ManyToOne(targetEntity=Adherent::class, inversedBy="contacts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $adherent;

    /**
     * @var string The firstname of this contact
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @var string The lastname of this contact
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @var string The email of this contact
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @var string|null The phone of this contact or null if not given
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string|null The office of this contact or null if not given
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $office;

    /**
     * @var bool If the ccntact is active or not
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @var \DateTimeInterface Creation date of this contact
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface Last time this contact's infos have been updated
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAdherent(): ?string
    {
        return $this->adherent;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }


    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }


    public function getOffice(): ?string
    {
        return $this->office;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }
}
